package com.ryan.argyle

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ArgyleSearchApplication : Application() {
}