package com.ryan.argyle.data


import com.google.gson.annotations.SerializedName

data class LinkItemDto(
    val features: Features,
    @SerializedName("has_two_fa")
    val hasTwoFa: Boolean,
    val id: String,
    @SerializedName("is_disabled")
    val isDisabled: Boolean,
    @SerializedName("item_id")
    val itemId: String,
    val kind: String,
    @SerializedName("known_limitations")
    val knownLimitations: Any,
    @SerializedName("logo_url")
    val logoUrl: String,
    val name: String,
    val status: String,
    @SerializedName("status_details")
    val statusDetails: Any,
    val type: String
)