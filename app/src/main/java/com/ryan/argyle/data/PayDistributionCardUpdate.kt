package com.ryan.argyle.data


data class PayDistributionCardUpdate(
    val supported: Boolean
)