package com.ryan.argyle.data


import com.google.gson.annotations.SerializedName

data class Features(
    @SerializedName("pay_distribution_card_update")
    val payDistributionCardUpdate: PayDistributionCardUpdate,
    @SerializedName("pay_distribution_update")
    val payDistributionUpdate: PayDistributionUpdate
)