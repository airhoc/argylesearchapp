package com.ryan.argyle.data


import com.google.gson.annotations.SerializedName

data class PayDistributionUpdate(
    @SerializedName("action_types")
    val actionTypes: List<Any>,
    @SerializedName("amount_allocation")
    val amountAllocation: Boolean,
    @SerializedName("amount_precision")
    val amountPrecision: String,
    @SerializedName("max_allocations")
    val maxAllocations: Int,
    @SerializedName("percent_allocation")
    val percentAllocation: Boolean,
    @SerializedName("percent_precision")
    val percentPrecision: Any,
    @SerializedName("required_fields")
    val requiredFields: RequiredFields,
    val supported: Boolean
)