package com.ryan.argyle.data


import com.google.gson.annotations.SerializedName

data class RequiredFields(
    @SerializedName("bank_account")
    val bankAccount: List<String>
)