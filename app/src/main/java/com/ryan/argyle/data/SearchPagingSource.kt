package com.ryan.argyle.data

import androidx.paging.PagingSource
import com.ryan.argyle.api.SearchApi
import retrofit2.HttpException
import java.io.IOException

private const val STARTING_PAGE_INDEX = 1

class SearchPagingSource(
    private val searchApi: SearchApi,
    private val query: String
) : PagingSource<Int, LinkItemDto>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, LinkItemDto> {
        val position = params.key ?: STARTING_PAGE_INDEX

        return try {
            val response = searchApi.searchItems(query)
            val links = response.results

            LoadResult.Page(
                data = links,
                prevKey = if (position == STARTING_PAGE_INDEX) null else position - 1,
                nextKey = if (links.isEmpty()) null else position + 1
            )
        } catch (exception: IOException) {
            LoadResult.Error(exception)
        } catch (exception: HttpException) {
            LoadResult.Error(exception)
        }
    }
}