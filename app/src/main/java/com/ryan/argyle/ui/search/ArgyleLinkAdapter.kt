package com.ryan.argyle.ui.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.ryan.argyle.R
import com.ryan.argyle.data.LinkItemDto
import com.ryan.argyle.databinding.ItemArgyleLinkBinding

class ArgyleLinkAdapter(private val listener: OnItemClickListener) :
    PagingDataAdapter<LinkItemDto, ArgyleLinkAdapter.LinkViewHolder>(ITEM_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LinkViewHolder {
        val binding =
            ItemArgyleLinkBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return LinkViewHolder(binding)
    }

    override fun onBindViewHolder(holder: LinkViewHolder, position: Int) {
        val currentItem = getItem(position)

        if (currentItem != null) {
            holder.bind(currentItem)
        }
    }

    inner class LinkViewHolder(private val binding: ItemArgyleLinkBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val item = getItem(position)
                    if (item != null) {
                        listener.onItemClick(item)
                    }
                }
            }
        }

        fun bind(link: LinkItemDto) {
            binding.apply {
                Glide.with(itemView)
                    .load(link.logoUrl)
                    .centerCrop()
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .error(R.drawable.ic_error)
                    .into(companyLogo)

                companyName.text = link.name
                companyKind.text = link.kind
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(link: LinkItemDto)
    }

    companion object {
        private val ITEM_COMPARATOR = object : DiffUtil.ItemCallback<LinkItemDto>() {
            override fun areItemsTheSame(oldItem: LinkItemDto, newItem: LinkItemDto) =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: LinkItemDto, newItem: LinkItemDto) =
                oldItem == newItem
        }
    }
}