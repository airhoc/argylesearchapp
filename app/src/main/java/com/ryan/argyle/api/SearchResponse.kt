package com.ryan.argyle.api

import com.ryan.argyle.data.LinkItemDto

data class SearchResponse(
    val results: List<LinkItemDto>
)