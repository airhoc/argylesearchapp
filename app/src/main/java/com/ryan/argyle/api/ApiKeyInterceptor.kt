package com.ryan.argyle.api

import com.ryan.argyle.BuildConfig
import okhttp3.*

class ApiKeyInterceptor : Interceptor {
    private var credentials: String = Credentials.basic(BuildConfig.ARGYLE_API_KEY, BuildConfig.ARGYLE_API_SECRET)

    override fun intercept(chain: Interceptor.Chain): Response {
        val original: Request = chain.request()
        val request: Request = original.newBuilder().header("Authorization", credentials).build()
        return chain.proceed(request)
    }
}