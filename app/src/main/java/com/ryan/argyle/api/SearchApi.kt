package com.ryan.argyle.api

import retrofit2.http.GET
import retrofit2.http.Query

interface SearchApi {

    companion object {
        const val BASE_URL = "https://api-sandbox.argyle.com/v1/"
        const val SEARCH_ITEM_LIMIT = 15
    }

    @GET("search/link-items")
    suspend fun searchItems(
        @Query("q") query: String,
        @Query("limit") limit: Int = SEARCH_ITEM_LIMIT
    ): SearchResponse
}