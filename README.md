# Argyle Search App

## Features
- Show an input field which captures search queries.
- Search results should be presented in a list, and dynamically appear with each keystroke (once at least 2 characters have been entered).
- Each search result should display Link Item with following properties: company logo, name, kind.

## Building
Add the following line to local.properties:
```
api_key_id=XXXXXX
api_key_secret=YYYYYY
```
where XXXXXX is your Argyle API key and YYYYYY is your Argyle API secret.

## Architecture
Application is built around MVVM and Clean Architecture concepts, utilizes such popular libraries as: LiveData, Hilt, Android Navigation Architecture Component, OkHttp, Retrofit, Glide.

## Libraries used
- AndroidX Navigation
- AndroidX Paging
- Android Hilt for dependency injection
- Retrofit2, OkHttp
- Glide
